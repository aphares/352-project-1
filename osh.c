#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <endian.h>
#include <ctype.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <limits.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <assert.h>
#include <sys/types.h>
#include <fcntl.h>

#define MAXLINE 80 /* The maximum length command */

/*
	Docuemntation: This is a program that, when launched, gives the user a prompt. The default case is to run the command in a child process with execvp (type == 0 is the default case). If there is a
	< or >, the value of type is set to 1 or 2 for input/output respectively. For input, the command accepts the input of a file as the arguments to the command (accept for sort, which uses the file
	location), and for output, all output of the command goes to a file that is specified in the prompt. There can only be one file listed for input and output. Finally, type == 3 when the user enters
	'|' for a pipe, which uses two child processes, a pipe child and pipe parent, to execute the two commands in a pipe.
	@author Andrew Phares
*/

int main(void)
{
    int length = MAXLINE / 2 + 1;
    char *args[length]; /* command line arguments */

    char buffer[1024];
    int shouldrun = 1; /* flag to determine when to exit program */
    int tinyFunction = 0;
    int numWords;
    char str[256];

    while (shouldrun)
    {
        printf("osh>");
        while (fgets(buffer, sizeof(buffer), stdin) != 0)
        {
            int i, j;
            int index = 0;
            int charCount;
            char *cmd = buffer;
            int cmdL[length];
            char word[length][256];

            while (sscanf(cmd, "%255s%n", word[index], &charCount) == 1) // charCount includes the space between elements in buffer
            {
                if (!index && strcmp(word[index], "!!") == 0)
                {
                    if (tinyFunction)
                    {
                        args[0] = str;
                        break;
                    }
                    else
                    {
                        printf("No commands in history\n");
                        break;
                    }
                }
                else if (!index) // set to null
                    for (i = 0; i < length; i++)
                        args[i] = NULL;
                if (!index && (strcmp(word[index], "exit") == 0 || strcmp(word[index], "stop") == 0)) // note: stop and exit are only valid as their own command.
                    return 0;

                args[index] = word[index];
                strcpy(str, word[0]);
                tinyFunction = 1;
                cmd += charCount;
                cmdL[index] = charCount;
                index++;
            }

            if (!tinyFunction)
                break;

            int ct = 0;
            int stat, cStat;
            int type = 0;

            // show current data in memory:
            for (i = 0; i < length && args[i]; i++)
            {
                // printf("args[%i]: %s - length: %i\n", i, args[i], cmdL[i]);
                // printf("type is %i\n", type);

                if (strcmp(args[i], "<") == 0)
                {
                    type = 1;
                    break;
                }
                else if (strcmp(args[i], ">") == 0)
                {
                    type = 2;
                    break;
                }
                else if (strcmp(args[i], "|") == 0)
                {
                    type = 3;
                    break;
                }
            }

            char *command[length];
            char *pipeCmd[length];
            for (int l = 0; l < length; l++)
            {
                command[l] = NULL;
                pipeCmd[l] = NULL;
            }
            int piper[2];

            if (type == 3)
            {
                printf("Beginning pipelining:\n");

                for (j = 0; j < i; j++)
                    command[j] = args[j];

                for (j = i + 1; j < index; j++)
                    pipeCmd[j] = args[j];

                if (pipe(piper) == -1)
                {
                    perror("Piping failed!");
                    exit(1);
                }

                if (fork() == 0) //first fork
                {
                    // printf("parent \n");
                    // for (j = 0; j < i; j++)
                    // {
                    //     printf("command[%i]: %s\n", j, pipeCmd[j]);
                    // }

                    dup2(piper[1], STDOUT_FILENO);
                    close(piper[0]);
                    close(piper[1]);

                    printf("We are executing parent command");
                    execvp(pipeCmd[0], pipeCmd);
                    perror("pipe parent's failure");
                    exit(1);
                }
                if (fork() == 0) //creating 2nd child
                {
                    // printf("child \n");
                    // for (j = i + 1; j < index; j++)
                    // {
                    //     printf("index is %i, pipeCmd[%i]: %s\n", index, j, command[j]);
                    // }

                    dup2(piper[0], STDIN_FILENO);
                    close(piper[1]);
                    close(piper[0]);

                    execvp(command[0], command);
                    perror("pipe child's failure");
                    exit(1);
                }

                close(piper[0]);
                close(piper[1]);
                wait(0);
                wait(0);
                break;
            }

            char buff[length][256];
            pid_t pid = fork();
            int fd;

            if (pid == 0)
            {
                //Child process
                switch (type)
                {
                case 0:
                    execvp(args[0], args);
                    printf("Unknown command\n");
                    exit(0);
                    break;

                case 1:
                    if (!args[i + 1] || args[i + 2])
                    {
                        printf("Invalid syntax: must have 1 and only 1 input file!\n");
                        break;
                    }
		    if (strcmp(args[0],"sort") == 0) {
			args[i] = args[i+1];
			args[i+1] = NULL;
		    execvp(args[0], args);
                    printf("Unknown command\n");
                    exit(0);

			break;
		    }
                    printf("Use %s as input to command\n", args[i + 1]);
                    fd = open(args[i + 1], O_RDONLY, 0600);
                    if (fd < 0)
                    {
                        fputs("Could not open file!\n", stderr);
                        perror("open");
                        exit(EXIT_FAILURE);
                    }
                    dup2(fd, STDIN_FILENO);
                    int ind = 1;
                    command[0] = args[0];
                    //printf("Vars -- cmdL:%i\n", cmdL[ind]);
                    while (fgets(buff[ind], 256, stdin))
                    {
                        //printf("Command[%i]: %s\n", ind, command[ind]);
                        // For my program, I have it set  to where 'echo < file.txt' outputs the contents of file.txt. This doesn't work with sort < file.txt, as that assumes a name to a file. The correct functionality for sort would require no handling of '<' -- what should the functionality for these commands be for our project?
                        command[ind] = buff[ind];
                        ind++;
                    }
                    //close(fd);
                    execvp(command[0], command);
                    exit(0);
                    break;

                case 2:
                    if (!args[i + 1] || args[i + 2])
                    {
                        printf("Invalid syntax: must have 1 and only 1 output file\n");
                        break;
                    }
                    printf("Writing output to new file %s\n", args[i + 1]);
                    fd = open(args[i + 1], O_WRONLY | O_CREAT | O_APPEND, 0600);
                    if (fd < 0)
                    {
                        fputs("Could not open file!\n", stderr);
                        perror("open");
                        exit(EXIT_FAILURE);
                    }
                    for (j = 0; j < i; j++)
                        command[j] = args[j];
                    dup2(fd, STDOUT_FILENO);
                    close(fd);
                    execvp(command[0], command);
                    exit(0);
                    break;
                }
            }
            else if (pid > 0)
            {
                // parent process
                waitpid(-1, &stat, 0);
                if (WIFEXITED(stat))
                    printf("Child process ended with status: %d\n\n", WEXITSTATUS(stat));
            }
            else
            {
                // fork failed
                printf("fork() failed!\n");
                return 1;
            }
            printf("osh>");
            fflush(stdout);
        }
    }
    return 0;
}
